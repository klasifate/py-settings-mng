import os
from pathlib import Path
from unittest import TestCase

from settings_mng import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    model_config = SettingsConfigDict(  # type: ignore[misc]
        BaseSettings.model_config,
        settings_modules="tests.config",
        env_file=Path(__file__).parent / ".env",
        secrets_dir=Path(__file__).parent,
    )

    init_arg: int
    env_arg: str
    dotenv_arg: float
    secrets_arg: int
    module_arg: str


class TestSettingsLoading(TestCase):
    def test(self) -> None:
        init_arg = 123
        env_arg = "some_string"
        dotenv_arg = 1.23
        secrets_arg = 124
        from .config import module_arg

        os.environ["env_arg"] = env_arg  # noqa: SIM112
        settings = Settings(init_arg=init_arg)  # type: ignore[call-arg]

        self.assertEqual(settings.init_arg, init_arg)
        self.assertEqual(settings.env_arg, env_arg)
        self.assertEqual(settings.dotenv_arg, dotenv_arg)
        self.assertEqual(settings.secrets_arg, secrets_arg)
        self.assertEqual(settings.module_arg, module_arg)
