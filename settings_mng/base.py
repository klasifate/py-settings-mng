from typing import Any, ClassVar, Type

from pydantic.fields import FieldInfo
from pydantic_settings import (
    BaseSettings as PydanticBaseSettings,
    PydanticBaseSettingsSource,
    SettingsConfigDict as PydanticSettingsConfigDict,
)
from pydantic_settings.sources import InitSettingsSource, EnvSettingsSource, DotEnvSettingsSource, SecretsSettingsSource

from .utils import ModulesAnnotation, read_settings


class SettingsConfigDict(PydanticSettingsConfigDict, total=False):
    settings_modules: ModulesAnnotation | None


class BaseSettings(PydanticBaseSettings):
    model_config: ClassVar[SettingsConfigDict] = SettingsConfigDict(  # type: ignore[misc]
        PydanticBaseSettings.model_config,
        extra="allow",
        validate_assignment=True,
        env_file="./.env",
        env_nested_delimiter="__",
        case_sensitive=True,
        settings_modules=None,
    )

    @classmethod
    def settings_customise_sources(  # type: ignore[override]
        cls,
        settings_cls: Type[PydanticBaseSettings],
        init_settings: InitSettingsSource,
        env_settings: EnvSettingsSource,
        dotenv_settings: DotEnvSettingsSource,
        file_secret_settings: SecretsSettingsSource,
    ) -> tuple[PydanticBaseSettingsSource, ...]:
        from_modules_settings = FromModulesSettingsSource(cls)

        return (
            init_settings,
            env_settings,
            dotenv_settings,
            file_secret_settings,
            from_modules_settings,
        )


class FromModulesSettingsSource(PydanticBaseSettingsSource):
    config: SettingsConfigDict

    def get_field_value(self, field: FieldInfo, field_name: str) -> tuple[Any, str, bool]:
        return None, "", False

    def __call__(self) -> dict[str, Any]:
        return read_settings(
            self.config["settings_modules"],
        )
