from typing import Any
from types import ModuleType
import importlib


ModulesAnnotation = list[tuple[str, str] | str] | tuple[str, str] | str


def extract(obj: Any, name: str) -> Any:
    if obj is None:
        raise ValueError("`None` is given")
    try:
        return getattr(obj, name)
    except AttributeError:
        pass
    try:
        return obj[name]
    except (TypeError, KeyError):
        return None


def get_obj(module: ModuleType, path: list[str]) -> Any:
    obj = module
    try:
        for part in path:
            obj = extract(obj, part)
    except ValueError as error:
        raise ValueError(
            f"Field not found in module `{module}` by the path `{path}`",
        ) from error
    return obj


def read_settings(  # noqa: C901
    modules: ModulesAnnotation | None,
) -> dict[str, Any]:
    if modules is None:
        return {}
    if isinstance(modules, (tuple, str)):
        modules = [modules]

    kwargs: dict[str, Any] = {}
    ordered_modules = modules[::-1]
    for module_info in ordered_modules:
        if isinstance(module_info, tuple):
            module_name, package_name = module_info
        else:
            module_name = module_info
            package_name = None

        if ":" in module_name:
            _ = module_name.split(":")
            if len(_) != 2:
                raise ValueError(f"Invalid path to settings: `{module_name}`")
            module_name, attr_name = _
        else:
            attr_name = None

        obj = importlib.import_module(module_name, package_name)
        if attr_name:
            obj = getattr(obj, attr_name, None)
            if obj is None:
                obj = {}
        if isinstance(obj, dict):
            kwargs.update(obj)
            continue
        for name in dir(obj):
            kwargs[name] = getattr(obj, name, None)

    return kwargs
